import requests
import sys
import argparse

server_name = 'https://api.vk.com/method/'

def send_request(method, params):
    try:
        return requests.get(server_name+method, params = params)
    except requests.exceptions.SSLError:
        print('SSL error')
        sys.exit(1)
    except requests.ConnectionError:
        print('Connection error or bad url')
        sys.exit(1)

def get_user_photos_max_likes(user_id, access_token):
    params = {'owner_id': user_id,
              'album_id':'profile',
              'extended' : 1,
              'access_token' : access_token,
              'v':5.74}

    json = send_request('photos.get', params).json()
    if 'response' in json:
        photos = json["response"]
        return f"Максимальное количество лайков на фото: " \
               f"{max(int(i['likes']['count']) for i in photos['items'])}"
    if 'error' in json:
        return f'Error! {json["error"]}'

def get_info_about_user(user_id, access_token):
    params = {'user_ids' : user_id,
              'fields' : 'about, bdate, city, quotes, status',
              'access_token' : access_token,
              'v' : 5.74}
    json = send_request('users.get', params).json()
    if 'response' in json:
        fields = json["response"][0]
        return f'Имя: {fields["first_name"]}\nФамилия: {fields["last_name"]}\n' \
               f'Дата рождения: {fields["bdate"]}\nГород: {fields["city"]["title"]}\n' \
               f'Обо мне: {fields["about"]}\nЛюбимые цитаты: {fields["quotes"]}\n' \
               f'Статус: {fields["status"]}\n'
    if 'error' in json:
        return f'Error! {json["error"]["error_msg"]}'

def get_user_groups(user_id, access_token, offset, count):
    params = {'user_id' : user_id,
              'access_token' : access_token,
              'v' : 5.74,
              'extended': 1,
              'offset' : offset,
              'count' : count}
    json = send_request('groups.get', params).json()
    if 'response' in json:
        fields = json['response']
        for item in fields['items']:
            return f"Название: {item['name']}\nТип сообщества: {item['type']}"
    if 'error' in json:
        return f'Error! {json["error"]}'

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('user_id', help='Enter user id')
    parser.add_argument('token', help='Enter access token')
    parser.add_argument('-g', nargs=2, help='Information about groups')
    parser.add_argument('-c', const=True, nargs='?', help='Common info')
    parser.add_argument('-p', const=True, nargs='?', help='Max photo likes')
    args = parser.parse_args()
    if args.p:
        print(get_user_photos_max_likes(args.user_id, args.token))
    if args.c:
        print(get_info_about_user(args.user_id, args.token))
    if args.g:
        print(get_user_groups(args.user_id, args.token, args.g[0], args.g[1]))


if __name__ == '__main__':
    main()
